# Copyright (C) 2019 all contributors <olddoc-public@80x24.org>
# License: GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0.txt>

# Loosely derived from Darkfish in the main rdoc distribution
require_relative 'oldweb'

# dark216 is an \RDoc template and not intended as a programming API.
# It respect prefers-color-scheme:light on newer browsers with CSS
# support, but favors darkness for power-savings.
# You may specify it as an \RDoc formatter:
#
#   rdoc -f dark216 ...
class Dark216 < Oldweb
  RDoc::RDoc.add_generator(self) # :nodoc:
  include ERB::Util # :nodoc:

  # description of the generator
  DESCRIPTION = 'minimal dark HTML generator'

  # default to a dark, web-safe (216 color) palette for power-savings.
  # Color-capable browsers can respect the prefers-color-scheme:light
  # @media query (browser support a work-in-progress)
  STYLE = <<'EOF'.gsub(/^\s*/m, '').delete!("\n") # :nodoc:
@media screen {
  *{background:#000;color:#ccc}
  a{color:#69f;text-decoration:none}
  a:visited{color:#96f}
}
@media screen AND (prefers-color-scheme:light) {
  *{background:#fff;color:#333}
  a{color:#00f;text-decoration:none}
  a:visited{color:#808}
}
EOF

  def initialize(*args) # :nodoc:
    super
    @oldweb_style = STYLE
  end
end
# :startdoc:
