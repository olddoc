# :stopdoc:
begin
  gem 'rdoc', [ '>= 4.2', '< 7.0' ]
  require_relative '../olddoc'
rescue Gem::LoadError
end unless defined?(Olddoc)
# :startdoc:
