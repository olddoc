# Copyright (C) 2015-2016 all contributors <olddoc-public@80x24.org>
# License: GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0.txt>
# helper methods for gemspecs
module Olddoc::Gemspec # :nodoc:
  include Olddoc::Readme

  def extra_rdoc_files(manifest)
    File.readlines('.document').map! do |x|
      x.chomp!
      if File.directory?(x)
        manifest.grep(%r{\A#{x}/})
      elsif File.file?(x)
        x
      else
        nil
      end
    end.flatten.compact
  end
end
