# Copyright (C) 2015-2016 all contributors <olddoc-public@80x24.org>
# License: GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0.txt>

require 'uri'
class Olddoc::Prepare # :nodoc:
  include Olddoc::NewsRdoc
  include Olddoc::NewsAtom
  include Olddoc::Readme

  def initialize(opts)
    rdoc_url = opts['rdoc_url']
    cgit_url = opts['cgit_url']
    rdoc_url && cgit_url or
      abort "rdoc_url and cgit_url required in .olddoc.yml for `prepare'"
    @rdoc_uri = Array(rdoc_url).map { |u| URI.parse(u) }
    @cgit_uri = Array(cgit_url).map { |u| URI.parse(u) }
    @name, @short_desc = readme_metadata
  end

  def run
    news_rdoc
    news_atom
  end
end
