# Copyright (C) 2015-2016 all contributors <olddoc-public@80x24.org>
# License: GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0.txt>

module Olddoc::NewsAtom # :nodoc:
  include Olddoc::History
  include Olddoc::Readme

  def x(dst, tag, text = nil, attrs = nil)
    if Hash === text
      attrs = text
      text = nil
    end
    if attrs
      attrs = attrs.map { |k,v| "#{k}=#{v.encode(xml: :attr)}" }
      attrs = "\n#{attrs.join("\n")}"
    end
    case text
    when nil
      if block_given?
        dst << "<#{tag}#{attrs}>"
        yield
        dst << "</#{tag}>"
      else
        dst << "<#{tag}#{attrs}/>"
      end
    else
      dst << "<#{tag}#{attrs}>#{text.encode(xml: :text)}</#{tag}>"
    end
  end

  # generates an Atom feed based on git tags in the document directory
  def news_atom_xml
    project_name, short_desc, _ = readme_metadata
    new_tags = tags[0,10]
    atom_uri = @rdoc_uri[0].dup
    atom_uri.path += "NEWS.atom.xml"
    news_uri = @rdoc_uri[0].dup
    news_uri.path += "NEWS.html"

    dst = ''
    x(dst, 'feed', xmlns: 'http://www.w3.org/2005/Atom') do
      x(dst, 'id', atom_uri.to_s)
      x(dst, 'title', "#{project_name} news")
      x(dst, 'subtitle', short_desc)
      x(dst, 'link', rel: 'alternate', type: 'text/html', href: news_uri.to_s)
      x(dst, 'updated', new_tags.empty? ? '1970-01-01:00:00:00Z'
                                        : new_tags[0][:time])
      new_tags.each do |tag|
        x(dst, 'entry') do
          x(dst, 'title', tag[:subject])
          x(dst, 'updated', tag[:time])
          x(dst, 'published', tag[:time])
          x(dst, 'author') do
            x(dst, 'name', tag[:tagger_name])
            x(dst, 'email', tag[:tagger_email])
          end
          uri = tag_uri(tag[:tag]).to_s
          x(dst, 'link', rel: 'alternate', type: 'text/html', href: uri)
          x(dst, 'id', uri)
          x(dst, 'content', type: 'xhtml') do
            x(dst, 'div', xmlns: 'http://www.w3.org/1999/xhtml') do
              x(dst, 'pre', tag[:body])
            end # div
          end # content
        end # entry
      end # new_tags.each
    end # feed
    [ dst, new_tags ]
  end

  def news_atom(dest = "NEWS.atom.xml")
    xml, new_tags = news_atom_xml
    File.open(dest, "w") { |fp| fp.write(xml) }
    unless new_tags.empty?
      time = new_tags[0][:ruby_time]
      File.utime(time, time, dest)
    end
  end
end
