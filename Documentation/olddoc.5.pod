=head1 NAME

.olddoc.yml - olddoc config file format

=head1 SYNOPSIS

A YAML file in the top-level project directory named ".olddoc.yml"

=head1 DESCRIPTION

As olddoc favors consistency over configuration, there is minimal
configuration to deal with.

=head1 KEYS

`rdoc_url`, `cgit_url`, `imap_url`, and `nntp_url` should be obvious

`merge_html` is a key-value mapping of (empty) RDoc source files to an
HTML file that will be merged into RDoc after-the-fact.  It is useful
for merging non-RDoc generated HTML into the project.

`ml_url` is the mail archive location.
`public_email` is the email address of a mail archived at `ml_url`

As of olddoc 1.2.0, `ml_url` and `nntp_url` may be YAML arrays
with multiple URLs.

As of olddoc 1.3.0, the `source_code` array may be any number of
commands or URLs.  This allows users of arbitrary version
control systems to specify instructions for getting the code
and not assume users are familiar with any particular system.

=head1 SEE ALSO

L<olddoc(1)>
