#!/usr/bin/ruby
# Copyright (C) 2019 all contributors <olddoc-public@80x24.org>
# License: GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0.txt>

# pod2html isn't to my liking, and we need to generate anchors
# compatible with what pandoc was generating to avoid breaking
# links.  Takes pod2text-generated text and transforms it to
# an HTML fragment

txts = ARGV
links = {}
txts.each do |f|
  if f =~ /(\A[\w\-]+)\.(\d)\.txt\z/
    base = $1
    section = $2
    links["#{base}(#{section})"] = "#{base}_#{section}.html"
  else
    abort "#{f} is not of <BASE>.<SECTION>.txt\n"
  end
end

linkre = links.keys.map { |x| Regexp.escape(x) }.join('|')

sections = '[A-Z][A-Z ]+'
txts.each do |f|
  str = File.read(f)
  str = str.split(/^(#{sections})$/mo)
  str = str.map! do |s|
    case s
    when /\A(#{sections})$/o
      # this is to be compatible with HTML fragments pandoc used
      sec = $1
      anchor = sec.downcase.tr(' ', '-')
      %Q(<h1\nid=#{anchor}>#{sec}</h1>)
    else
      s.encode!(xml: :text)
      s.gsub!(/\b(#{linkre})/mo) do |m|
        manref = $1
        if url = links[manref]
          %Q(<a\nhref="#{url}">#{manref}</a>)
        else
          manref
        end
      end
      s.rstrip!
      s.empty? ? '' : "<pre>#{s}</pre>"
    end # case s
  end.join

  html = f.sub(/.txt\z/, '.html')
  tmp = html + '+'
  File.open(tmp, 'w') { |f| f.write(str) }
  File.rename(tmp, html)
end
