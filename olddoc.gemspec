# Copyright (C) 2015-2016 all contributors <olddoc-public@80x24.org>
# License: GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0.txt>
$LOAD_PATH << 'lib'
require 'olddoc'
require 'olddoc/version'
extend Olddoc::Gemspec
name, summary, title = readme_metadata
Gem::Specification.new do |s|
  manifest = File.read('.gem-manifest').split(/\n/)
  s.name = 'olddoc'
  s.version = Olddoc::VERSION
  s.executables = %w(olddoc)
  s.authors = ["#{s.name} hackers"]
  s.summary = summary
  s.description = readme_description
  s.email = 'e@80x24.org'
  s.files = manifest

  # works fine with RDoc 5.x
  s.add_dependency('rdoc', ['>= 4.2', '< 7.0'])
  s.required_ruby_version = '>= 1.9.3'
  s.homepage = Olddoc.config['rdoc_url']
  s.licenses = 'GPL-3.0+'
end
